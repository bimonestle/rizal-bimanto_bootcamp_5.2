import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  constructor(private api:ApiService) { }

  event:string = "";
  date:string = "";
  price:string = "";

  ngOnInit() {
    this.api.getEventList()
    .subscribe(result => this.event = result);
    
  }

  buyTicket(){
    this.api.buyTicket(this.event)
    .subscribe(result => this.event = result);
  }

}
