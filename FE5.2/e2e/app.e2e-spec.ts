import { FE5.2Page } from './app.po';

describe('fe5.2 App', () => {
  let page: FE5.2Page;

  beforeEach(() => {
    page = new FE5.2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
