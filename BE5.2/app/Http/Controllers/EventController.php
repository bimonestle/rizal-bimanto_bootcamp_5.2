<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;

class EventController extends Controller
{
    public function getEvent(){
        $allEvent = Event::get();
        return response()->json($allEvent, 200);
    }
    public function addEvent(){
        DB::beginTransaction();
        try{
        $this->validate($request, [
            'EventName' => 'required',
            'EventDate' => 'required',
            'Price' => 'required'
        ]);

        $eventname = $request->input('EventName');
        $eventdate = $request->input('EventDate');
        $eventprice = $request->input('Price');

        $event = new Event;
        $event->name = $eventname;
        $event->date = $eventdate;
        $event->price = $eventprice;
        $event->save();

        $evn = EventList::get();

        DB::commit();
        return response()->json($evn, 200);
        }

        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}
