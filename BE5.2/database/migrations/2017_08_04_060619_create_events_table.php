<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('EventName');
            $table->integer('JadwalID')->unsigned();
            $table->foreign('JadwalID')->references('id')->on('jadwals');
            $table->integer('HargaID')->unsigned();
            $table->foreign('HargaID')->references('id')->on('hargas');
            $table->integer('TiketID')->unsigned();
            $table->foreign('TiketID')->references('id')->on('tikets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
